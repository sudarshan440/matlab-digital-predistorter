
%********************** preparation part ***************************
% this is the main program for adaptive DPD for compensating RF impairments


   
%*********************LTE generation*****************************

   clear all;
   clc; close all;
   
   signal = OFDM_TX_FRAME(2048,1447,512,12,1) ; % function generates the 5MHz OFDM signal

% initial LMS weights for Quadrature modulator error compensation      
d_1 = 1; 
d_2 = 0;
d_3 = 0;
b_q_m = [d_1, d_2, d_3]'; % In equation(6) bqm = [d_1,d_2,d_3]]' (under assumtion of ideal power amplifier)from paper "Joint adaptive compensation for ...
% amplifier nonlinearity and quadrature modulation errors, 2006 IEEE (young-Doo-Kim)"   

power = mean(abs(signal).^2); % input power
    backoff_dB = 12.5; % back off in linear scale
    power_in_dB = 10*log10(power);  %  Back Off
    fact_norm = sqrt(10^(-(backoff_dB+ power_in_dB)/10)); 
% modulator errors variables
gain_imb = .3 ;  % gain imabalance of modulator in linear scale
phase_imb = 10;     % phase imbalance of modulator in degrees
dc_offset = 0.05 + 1i*0.05 ;  % dc_offset
        
modulator_in = signal*fact_norm; % variable for storing input signal

% modulator errors/demodulator errors/RF impairments before compensation 
  PA_input_before_compensation = modulator(modulator_in,gain_imb,phase_imb,dc_offset); % modulator function for introducing modulator errors 
  PA_output_before_compensation = pa_before_compensation(PA_input_before_compensation,3); % power amplifier function
  demod_output = demodulator(PA_output_before_compensation,gain_imb,phase_imb,dc_offset); % demodulator function for introducing demodulator errors
% plotting of figures 


 % plotting of spectrum with out compensation
        Fs= 7.68e6;    
        hPsd2 = spectrum.welch('Blackman',2048);
        hopts2 = psdopts(hPsd2);
        set(hopts2,'SpectrumType','twosided','NFFT',2048,'Fs',Fs,...,
        'CenterDC',true); 
        PSD3 = psd(hPsd2,demod_output,hopts2);
        data = dspdata.psd([PSD3.Data],PSD3.Frequencies,'Fs',Fs); figure(9);
        plot(data);
        legend('PSD before compensation of RF impairments');
        
Vmax = max(abs(modulator_in));
EVM_before_compensation_RF_impairments = 0;
EVM_before_compensation_RF_impairments = EVM_before_compensation_RF_impairments + mean(abs(demod_output  - modulator_in).^2);
EVM_before_compensation_RF_impairments = sqrt(EVM_before_compensation_RF_impairments)/ Vmax;

     figure(1);
        plot((((abs(demod_output).^2))),(((abs(modulator_in).^2))),'r'); 
  
        title('AM/AM curve before QM and QDM compensation')
        xlabel('input');
        ylabel('output'); hold on;
    
        figure(2)
        phasediff3 = 360/(2*pi)*(wrapToPi(angle(demod_output)-angle(modulator_in))); 
        plot((((abs(modulator_in).^2))),phasediff3,'r');
      
        title('AM/PM curve before QM and QDM compensation')
        xlabel('input');
        ylabel('phase'); hold on;

% % initilization of variables before compensation
 filter_length =50; % assumtion of Hilbert Filter length 
modulator_in = [zeros(1,filter_length),modulator_in];  % adding zeros before input for delayed input signal (this is needed for Hilbert Filter)    
QM_output_1sample_per_iteration = zeros(1,length(signal)+filter_length); % Quadrature modulator(QM)output 
%  PA_output = zeros(1,length(signal)+filter_length+1); % power amplifier(PA) function
QMC_training_input= zeros(1,length(signal)+filter_length); 
QMC_training_output= zeros(1,length(signal)+filter_length); 
mixer_out = zeros(1,length(signal)+filter_length);
input = zeros(1,filter_length); % input signal for Hilbert Filter
gain_factor = 1;   



% % % %************** main loop for adaptive QM and QDM for compensating QM and QDM errors ******************************************
% % %adaptation is done by sample to sample comparision

 for y =filter_length + 1:1:length(signal)+filter_length % begining of FOR loop y = filter_length + 1 (since number of zeros added = length of hilbert filter before input signal)

   QM_in = modulator_in(y);  % current input
               
   [QM_output_1sample_per_iteration(y),u_q] = quadrature_modulator_compensation(QM_in,b_q_m); % Quadrature modulator/ (QM and QDM compensator)

   [mixer_out(y)  beta alpha] = modulator_for1sample(QM_output_1sample_per_iteration(y),gain_imb,phase_imb,dc_offset); % modulator error for adding modulator errors (gain,phase errors and dc offset)
   mixer_out1(y) = (mixer_out(y)/gain_factor).*exp(1j*2*pi*(y-filter_length-1)*(1/4)); % assuming that after demodulation the frequency difference between Tx_pll and Rx_pll is 1/4
   
  
% Taking 50 values (equal to length of hilbert  filter) as input for hilbert filter
%    
  for i1 = y-filter_length:1:y
    input(i1) =  mixer_out1(i1);
  end
 
%     [QMC_training_input(y) ] = demodulator_for1sample(input,y); % when ideal demodulator errors
     [QMC_training_input(y)] = demodulator_for1sample(input,gain_imb,phase_imb,dc_offset,y); % when demodulator errors exists
       
              
 [QMC_training_output(y),U] = QMC_training( QMC_training_input(y),b_q_m);  % quadrature modulator correcter block   
         
 signal_delayed =  QM_output_1sample_per_iteration(y);  

 PD_out = QMC_training_output(y); 

%********************* LMS algorithm******************** 
      
 [e_RF_impairements b_q_m] = lmsalogorithm(signal_delayed,U,b_q_m); % when considering IQ imbalances and its compensation
 
 error1(y) = e_RF_impairements;
 weights1(y) = b_q_m(1);  weights2(y) = b_q_m(2); weights3(y) = b_q_m(3);
 % just for checking if program is running or not

     if(mod(y,500)==0)
        disp(y)
       % value of 1st row of b_q_m
       disp(b_q_m(1))
     end

 end % end of FOR loop
 
 
% Learning curvature 
    
E = zeros(1,length(mixer_out1)); E(1) = 1;
L = 0.999;

for n = 2:1:length(mixer_out1)
E(n) = (L)*E(n-1) + (1-L)*(abs((error1(n)).*(error1(n)))); 
end 
figure(7);
plot(10*log10(E));
title('learning curvature for QM and QDM Compensation');
xlabel('number of samples');
ylabel('mean square error');


% ploting of spectrum    
    
Fs= 7.68e6;    
hPsd = spectrum.welch('Blackman',2048);
hopts = psdopts(hPsd);
set(hopts,'SpectrumType','twosided','NFFT',2048,'Fs',Fs,...,
'CenterDC',true);
PSD1 = psd(hPsd,signal,hopts);
PSD2 = psd(hPsd,0.1.*mixer_out(51:end),hopts);

data = dspdata.psd([PSD1.Data PSD2.Data ],PSD1.Frequencies,'Fs',Fs); figure(9);
plot(data);
legend('Ideal Signal', 'after QM and QDM Compensation');

% EVM after compensation 

Vmax = max(abs(modulator_in));
EVM_after_compensation_QMandQDM_compensation = 0;
EVM_after_compensation_QMandQDM_compensation = EVM_after_compensation_QMandQDM_compensation + mean(abs(mixer_out(51:end) - modulator_in(51:end)).^2);
EVM_after_compensation_QMandQDM_compensation = sqrt(EVM_after_compensation_QMandQDM_compensation)/ Vmax;

% AM-AM curve     

figure(3);
        plot((((abs(mixer_out(51:end)).^2))),(((abs(QM_output_1sample_per_iteration(51:end)).^2))),'.'); 
        title('AM/AM curve after QM and QDM compensation')
        xlabel('input');
        ylabel('output');hold on;
 % AM-PM curve       
        figure(4)
        phasediff3 = 360/(2*pi)*(wrapToPi(angle(mixer_out(51:end))-angle(QM_output_1sample_per_iteration(51:end)))); 
        plot((((abs(QM_output_1sample_per_iteration(51:end)).^2))),phasediff3,'.');
        title('AM/PM curve after QM and QDM compensation')
        xlabel('input');
        ylabel('output')
        hold on;
     


        
% % % %%%************************ power amplifier nonlinearity compensation *****
pamodel_afterloopback = 3;
power = mean(abs(demod_output).^2); 
% % initial weights for LMS for compensation of power amplifier nonlinearity
weights_afterloopback = zeros(9,1);
weights_afterloopback(1)=1;

    PA_input_afterloopback = (mixer_out(51:end)); 

    PA_input_afterloopback = [zeros(1,2), PA_input_afterloopback ]; % adding 2 zeros before assuming that memory polynomial PA of memory depth = 2
    PD_output_1_afterloopback = zeros(1,30722) ;
    postdistorter_out_afterloopback = zeros(1,30722);
    PA_output_afterloopback = zeros(1,30722);
    
 
 for x = 3:1:length(signal)+2 % FOR loop 

    %%% sample to sample adaptation
    
    predistorter_afterloopback =   PA_input_afterloopback(x); % current input
    predistorter_afterloopback_in_2 =  PA_input_afterloopback(x-1); % delayed input initially it is zero for 1st sample
    predistorter_afterloopback_in_3 = PA_input_afterloopback(x-2); % delayed input initially it is zero for 2nd sample
                
    [PD_output_1_afterloopback(x),u_q] = predistorter_demod(predistorter_afterloopback,predistorter_afterloopback_in_2,predistorter_afterloopback_in_3,weights_afterloopback); % predistortion function 
        
     PA_in_1_afterloopback = PD_output_1_afterloopback(x); % 1st sample
     PA_in_2_afterloopback = PD_output_1_afterloopback(x-1); % 2nd sample
     PA_in_3_afterloopback =PD_output_1_afterloopback(x-2); % 3rd sample
       
     [PA_output_afterloopback(x)] = poweramplifier(PA_in_1_afterloopback,PA_in_2_afterloopback,PA_in_3_afterloopback,pamodel_afterloopback ); % power amplifier function
        
     postdistorter_in_1_afterloopback = PA_output_afterloopback(x); % input(1st sample)  of post distorter
     postdistorter_in_2_afterloopback = PA_output_afterloopback(x-1); % input(2nd sample) of post distorter  
     postdistorter_in_3_afterloopback = PA_output_afterloopback(x-2); % input(3rd sample) of post distorter 
    
     [postdistorter_out_afterloopback(x),u_k_q_post_D] = postdistorter(postdistorter_in_1_afterloopback, postdistorter_in_2_afterloopback, postdistorter_in_3_afterloopback,weights_afterloopback); % postdistorter

 % % ******************* LMS algorithm******************************* 
         
       signal_delayed_afterloopback = PD_output_1_afterloopback(x); % signal after predistorter and QMC block

       [e weights_afterloopback] = lmsalogorithm(signal_delayed_afterloopback,u_k_q_post_D,weights_afterloopback); % [error weights] = lmsalgorithm(1st i/p, 2nd i/p, weights)

    error_PA_compensation(x) = e;
    
    weights4(x) = weights_afterloopback(1);    weights5(x) = weights_afterloopback(2);    weights6(x) = weights_afterloopback(3); 
       weights7(x) = weights_afterloopback(4);    weights8(x) = weights_afterloopback(5);    weights9(x) = weights_afterloopback(6); 
          weights10(x) = weights_afterloopback(7);    weights11(x) = weights_afterloopback(8);    weights12(x) = weights_afterloopback(9); 
    if(mod(x,500)==0)
        disp(x)
        disp(weights_afterloopback(1))
    end

 end % end of FOR loop


 % MSE curve
 
 E = zeros(1,length(PA_output_afterloopback)); E(1) = 1;
L = 0.999;

for n = 2:1:length(PA_output_afterloopback)
E(n) = (L)*E(n-1) + (1-L)*(abs(( error_PA_compensation(n)).*( error_PA_compensation(n)))); 
end

figure(8);
plot(10*log10(E));
title('learning curvature for RF Impairments compensation');
xlabel('number of samples');
ylabel('mean square error');
    
Fs= 7.68e6;    
hPsd = spectrum.welch('Blackman',2048);
hopts = psdopts(hPsd);
set(hopts,'SpectrumType','twosided','NFFT',2048,'Fs',Fs,...,
'CenterDC',true);

PSD5 = psd(hPsd, 0.1.*PA_output_afterloopback(3:end),hopts);
data = dspdata.psd([ PSD1.Data PSD2.Data PSD5.Data ],PSD1.Frequencies,'Fs',Fs); figure(12);
plot(data); 
legend('ideal signal','after QM and QDM compensation','after RF impairments compensation');

% EVM after compensation of RF impairments

Vmax = max(abs(modulator_in));
EVM_after_compensation_RF_impairments = 0;
EVM_after_compensation_RF_impairments = EVM_after_compensation_RF_impairments + mean(abs(PA_output_afterloopback(3:end) - modulator_in(51:end)).^2);
EVM_after_compensation_RF_impairments = sqrt(EVM_after_compensation_RF_impairments)/ Vmax;


% AM-AM curve 
        figure(5);
        plot((((abs(PA_output_afterloopback(3:end)).^2))),(((abs(PD_output_1_afterloopback(3:end)).^2))),'.'); 
        hold on;
        title('AM/AM curve after compensation of RF impairments');
        xlabel('normalized PA Input');
        ylabel('normalized PA output')
% AM-PM curve         
        figure(6)
        phasediff3 = 360/(2*pi)*(wrapToPi(angle(PA_output_afterloopback(3:end))-angle(PD_output_1_afterloopback(3:end)))); 
        plot((((abs(PD_output_1_afterloopback(3:end)).^2))),phasediff3,'.');
        hold on;
        title('AM/PM curve after compensation of RF impairments');
        xlabel('normalized PA Input');
        ylabel('PA phase')


    