function [mixer_out beta alpha] = modulator_QMC(mixer_in_1,gainimb,phaseimb,dc_offset,y)


filter_length = 100;
phaseimb_linear = phaseimb/180 *pi;

beta = (1/2)*(1 + (1+ gainimb) * exp(1i*phaseimb_linear));
alpha = (1/2)*(1 - (1+ gainimb) * exp(-1i*phaseimb_linear));
% 
% if y == 3
%     
%     dc_1 = 0; dc_2 = 0; % since for 1st sample delayed signals doesnt have any dc offset
% elseif y == 4
%     
%        dc_1 = dc_offset; dc_2 = 0; % At 2st sample 
% else
%        dc_1 = dc_offset; dc_2 = dc_offset; % for rest of the samples
% end
mixer_out = (beta .* mixer_in_1 + alpha .* conj(mixer_in_1) + dc_offset).*((exp(1j*2*pi*(y-filter_length-1)*(1/4))));


