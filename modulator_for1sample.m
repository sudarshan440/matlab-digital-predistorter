function [mixer_out beta alpha] = modulator_for1sample(mixer_in,gainimb,phaseimb,dc_offset)



phaseimb_linear = phaseimb/180 *pi; % phaser imb in radians 
% equations of beta and alpha are taken form equation (2) in paper "Joint adaptive compensation for ...
% amplifier nonlinearity and quadrature modulation errors, 2006 IEEE (young-Doo-Kim)" 

beta = (1/2)*(1 + (1+ gainimb) * exp(1i*phaseimb_linear));
alpha = (1/2)*(1 - (1+ gainimb) * exp(-1i*phaseimb_linear));

mixer_out = (beta .* mixer_in + alpha .* conj(mixer_in) + dc_offset);



