function [mixer_out] = demodulator(mixer_in,gainimb,phaseimb,dc_offset)


phaseimb_radians = phaseimb/180 *pi; % phase imbalance in radians

% equations of beta and alpha are taken form equation (2) in paper "Joint adaptive compensation for ...
% amplifier nonlinearity and quadrature modulation errors, 2006 IEEE (young-Doo-Kim)" 

beta = (1/2)*(1 + ((1+ gainimb) * exp(1i*phaseimb_radians)));
alpha = (1/2)*(1 - ((1+ gainimb) * exp(-1i*phaseimb_radians)));

% adding demodulator errors whos gain,phase and dc offset values are same
% as modulator

mixer_out = beta .* mixer_in + alpha .* conj(mixer_in) + dc_offset;

