%************************ Mixer ***************************



function [mixer_out] = modulator(mixer_in,gainimb,phaseimb,dc_offset)


phaseimb_radians = phaseimb/180 *pi; % phase imabalance in radians

% equations of beta and alpha are taken form equation (2) in paper "Joint adaptive compensation for ...
% amplifier nonlinearity and quadrature modulation errors, 2006 IEEE (young-Doo-Kim)" 

beta = (1/2)*(1 + ((1+ gainimb) * exp(1i*phaseimb_radians)));
alpha = (1/2)*(1 - ((1+ gainimb) * exp(-1i*phaseimb_radians)));

n = 0:30719; % as number of input samples = 30720
mixer_out = (beta .* mixer_in + alpha .* conj(mixer_in) + dc_offset).*exp(1j*2*pi*(1/4)*n);



