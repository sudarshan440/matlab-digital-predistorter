function [PD_out,u_k_q_PD] = quadrature_modulator_compensation(QMC_in,b_q_m)


u_k_q_PD = QMC_in;
u_k_q_PD = [u_k_q_PD', u_k_q_PD.', 1]'; 
PD_out= b_q_m'*u_k_q_PD; % equation 6 in "Joint adaptive compensation for ...
% amplifier nonlinearity and quadrature modulation errors, 2006 IEEE (young-Doo-Kim)












