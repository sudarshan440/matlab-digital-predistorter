function [QMC_out,u_k_q_Post_D] = QMC_training(QMCtraining_in,b_q_m)

 
u_k_q_Post_D = [QMCtraining_in', QMCtraining_in.', 1]';
QMC_out= b_q_m'*u_k_q_Post_D; % equation (6) from paper "Joint adaptive compensation for ...
% amplifier nonlinearity and quadrature modulation errors, 2006 IEEE % (young-Doo-Kim)"   


