
 function [PA] = pa_before_compensation(PA_input,pamodel)

 % when pamodel =1 PA is saleh, pamodel =2 PA is rapp, pamodel = 3 PA is
 % memory polynomial and when pamodel = 4 PA is power amplifier
 
     amplitude = abs(PA_input); % amplitude of power amplifier input
     theta = angle(PA_input);  % phase of power amplifier input



   
 switch pamodel
    case 1   
% % % 1. TWTA (Traveling-Wave Tube Amplifier) Model

% this values are taken from [11]
      Alpha_AM = 2.1587;
        Alpha_PM = 4.033; 
        Beta_AM = 1.1517; 
        Beta_PM = 9.104;
      
        Gain  = (Alpha_AM * amplitude)./ (1 + Beta_AM.*(amplitude.*amplitude)); % gain equation
        Phase1  =  (Alpha_PM * (amplitude.*amplitude))./ (1 + Beta_PM.*(amplitude.*amplitude)); % phase equation
        totaltheta = Phase1 + theta;
        PA = Gain .* exp(1j*totaltheta); % for equation refer to 2.3.1 in documentation
%   disp('saleh power amplifier model');


    case 2
        
        % rapp power amplifier model
        
          
    disp('Rapp power amplifier model');
    A0 = 1; % maximum output amplitude 
    p = 3; % parameter that affects the smoothness of transistion
    PA = 1./ ((1+ (amplitude ./A0).^(2*p)).^(1/(2*p))); % calculates the amplitude of power amplifier

    
     
     case 3

%********************** memory polynomial model ***************************


% output of power amplifier

p=5; % polynomial co-efficient
q=2; % memory depth

% co-efficients of memory power amplifier
a10 = 1.0513+0.0904*1j;
a11 = -0.0680-0.0023*1j;
a12 = 0.0289-0.0054*1j;
a30 = -0.0542-0.2900*1j;
a31 = 0.2234+0.2317*1j;
a32 = -0.0621-0.0932*1j;
a50 = -0.9657-0.7028*1j;
a51 = -0.2451-0.3735*1j;
a52 = 0.1229+0.1508*1j;

A = [ [a10 a30 a50]  [a11 a31 a51]  [a12 a32 a52] ]; A= A.';

%       
memory = q+1; 
X = [zeros(memory-1,1); PA_input.' ; zeros(memory-1,1)];


signal_out = 0;
P_vect = 1:1:p-2;
Q_vect = 1:1:q+1;
k=2;

for bcle_Q = Q_vect
    for bcle_P = P_vect
  
       signal_out = signal_out + A(k-1,:)*circshift(X,bcle_Q-1).* abs(circshift(X,bcle_Q-1)).^(2*(bcle_P-1)); 
       k = k+1;
           
    end
end

PA = signal_out(memory:end-memory+1).';


     case 4
         
         % ideal power amplifier
         
      A = [ [1 0 0]  [0 0 0]  [0 0 0] ]; A= A.'; % co-efficinets for ideal power amplifier akq = 0 for q! = 0, a10 = 1 and ak0 = 0 for k! =0

      signal_out = 0;
      X = [zeros(2,1); PA_input ; zeros(2,1)];
      k=2;
for bcle_Q = 1:1:3
    for bcle_P = 1:1:3
      
      signal_out = signal_out + A(k-1,:)*circshift(X,bcle_Q-1).* abs(circshift(X,bcle_Q-1)).^(2*(bcle_P-1)); k = k+1;
      
    end
end
      PA = signal_out(3:end-2);
 
 end
    
           

