function [PostD_out,u_k_q_post_D] = postdistorter( postdistorter_in_1, postdistorter_in_2, postdistorter_in_3,weights_post_D)

   
%********************** memory polynomial model ***************************

       


p = 5 ; % polynomial order
q = 2; memory = q+1;


signal_out = zeros(1,9);

mean_gain_tmp1 = 1;

% signal_out =  [ a(n),  a(n)|a(n)|^2, a(n)|a(n)|^4, a(n-1),  a(n-1)|a(n-1)|^2,a(n-1)|a(n-1)|^4,  a(n-2),  a(n-2)|a(n-2)|^2, a(n-2)|a(n-2)|^4 ]^T
       signal_out(1,1) = postdistorter_in_1/mean_gain_tmp1.* (abs((postdistorter_in_1)./mean_gain_tmp1).^(2*(0)));
       signal_out(1,2) = postdistorter_in_1/mean_gain_tmp1.* (abs((postdistorter_in_1)./mean_gain_tmp1).^(2*(1)));
       signal_out(1,3) = postdistorter_in_1/mean_gain_tmp1.* (abs((postdistorter_in_1)./mean_gain_tmp1).^(2*(2)));
       
       signal_out(1,4) = postdistorter_in_2/mean_gain_tmp1.* (abs((postdistorter_in_2)./mean_gain_tmp1).^(2*(0)));
       signal_out(1,5) = postdistorter_in_2/mean_gain_tmp1.* (abs((postdistorter_in_2)./mean_gain_tmp1).^(2*(1)));
       signal_out(1,6) = postdistorter_in_2/mean_gain_tmp1.* (abs((postdistorter_in_2)./mean_gain_tmp1).^(2*(2)));
       
       signal_out(1,7) = postdistorter_in_3/mean_gain_tmp1.* (abs((postdistorter_in_3)./mean_gain_tmp1).^(2*(0)));
       signal_out(1,8) = postdistorter_in_3/mean_gain_tmp1.* (abs((postdistorter_in_3)./mean_gain_tmp1).^(2*(1)));
       signal_out(1,9) = postdistorter_in_3/mean_gain_tmp1.* (abs((postdistorter_in_3)./mean_gain_tmp1).^(2*(2)));
       
       
       
       
   
u_k_q_post_D = [signal_out(1,1),  signal_out(1,2), signal_out(1,3), signal_out(1,4), signal_out(1,5),signal_out(1,6), signal_out(1,7) signal_out(1,8), signal_out(1,9) ];


u_k_q_post_D = u_k_q_post_D.';
 
PostD_out = weights_post_D'*u_k_q_post_D ; % from equation (9) in paper "A memory polynomial predistorter for compensation of nonlinearity with...
%memory effects in WCDMA transmitters, 2009 IEEE" 



