
% function [output ] = demodulator_for1sample(input,y) % for ideal % demodulator errors

function [output ] = demodulator_for1sample(input,gainimb,phaseimb,dc_offset,y) % inclusion of demodulator errors

filter_length = 50;
x = fliplr(input); % fliping since the present signal starts from (filter length + 1) column
input_filter = x(1:end-1);  % present input
phaseimb_radians = phaseimb/180 *pi;
% % equations of beta and alpha are taken form equation (2) in paper "Joint adaptive compensation for ...
% % amplifier nonlinearity and quadrature modulation errors, 2006 IEEE (young-Doo-Kim)" 

beta = (1/2)*(1 + ( (1+gainimb) * exp(1i*phaseimb_radians)));
alpha = (1/2)*(1 - ( (1 +  gainimb) * exp(-1i*phaseimb_radians)));

z_n_1 =  (beta*input_filter +alpha*conj(input_filter)+dc_offset); % demodulator errors 



% hilbert filter is used for removing errors caused by demodulator and
% selecting the required band
A=firls(49,[0 0.2 0.25 0.75 0.8 1 ],[0 0 1 1 0 0 ]);
B=firls(49,[0 0.2 0.25 0.75 0.8 1 ],[0 0 1 1 0 0 ],'hilbert');
C= A+1i*B; 

for i1 = 1:1:50
    
    y1(i1) = C(i1)*z_n_1(i1);  
%     y1(i1) = C(i1)*input_filter(i1);  % for ideal demodulator 
   
end

y2 = sum(y1); %y(n) = c_0*input_filter(n) + c_1 *input_filter(n-1) + c_2 *input_filter(n-2) +......

shifting_tone = exp(1i*2*pi*(-1/4)*(y-filter_length-1)); % for shifting the tones to origin
output = y2*shifting_tone;
% since, after loop back the input signal is shifted by 1/4 as difference
% between Tx pll abd Rx pll. so for shifting the tone back to original position it
% should be multiplied with (-1/4)


