

 function [PA_out] = poweramplifier(PA_in,PA_in2,PA_in3,pamodel)

     amplitude = abs(PA_in);
     theta = angle(PA_in);
   
 switch pamodel
    case 1   
% % % 1. TWTA (Traveling-Wave Tube Amplifier) Model

  
        Alpha_AM = 2.1587;
        Alpha_PM = 4.033; 
        Beta_AM = 1.1517; 
        Beta_PM = 9.104;

        Gain  = (Alpha_AM * amplitude)./ (1 + Beta_AM.*(amplitude.*amplitude)); % gain equation
        Phase1  =  (Alpha_PM * (amplitude.*amplitude))./ (1 + Beta_PM.*(amplitude.*amplitude)); % phase equation
        totaltheta = Phase1 + theta;
        PA_out = Gain .* exp(1i*totaltheta);
%   disp('saleh power amplifier model');

%     

    case 2
        
        % rapp power amplifier model
       

    A0 = 1; % maximum output amplitude 
    p = 3; % parameter that affects the smoothness of transistion
    PA_out  = 1/ ((1+ (amplitude ./A0).^(2*p)).^(1/(2*p))); % calculates the amplitude of power amplifier

    
     
     case 3

%********************** memory polynomial model ***************************


a10 = 1.0513+0.0904*1j;
a11 = -0.0680-0.0023*1j;
a12 = 0.0289-0.0054*1j;
a30 = -0.0542-0.2900*1j;
a31 = 0.2234+0.2317*1j;
a32 = -0.0621-0.0932*1j;
a50 = -0.9657-0.7028*1j;
a51 = -0.2451-0.3735*1j;
a52 = 0.1229+0.1508*1j;


A = [ [a10 a30 a50]  [a11 a31 a51]  [a12 a32 a52] ]; A= A.';

signal_out = zeros(1,9);
% signal_out =  [ a(n),  a(n)|a(n)|^2, a(n)|a(n)|^4, a(n-1),  a(n-1)|a(n-1)|^2,a(n-1)|a(n-1)|^4,  a(n-2),  a(n-2)|a(n-2)|^2, a(n-2)|a(n-2)|^4 ]^T
       signal_out(1,1) = PA_in* (abs((PA_in)).^(2*(0)));
       signal_out(1,2) = PA_in* (abs((PA_in)).^(2*(1)));
       signal_out(1,3) = PA_in* (abs((PA_in)).^(2*(2)));
       
       signal_out(1,4) = PA_in2* (abs((PA_in2)).^(2*(0)));
       signal_out(1,5) = PA_in2* (abs((PA_in2)).^(2*(1)));
       signal_out(1,6) = PA_in2* (abs((PA_in2)).^(2*(2)));
       
       signal_out(1,7) = PA_in3* (abs((PA_in3)).^(2*(0)));
       signal_out(1,8) = PA_in3* (abs((PA_in3)).^(2*(1)));
       signal_out(1,9) = PA_in3* (abs((PA_in3)).^(2*(2)));
       
       
  
u_k_q = [signal_out(1,1),  signal_out(1,2), signal_out(1,3), signal_out(1,4), signal_out(1,5),signal_out(1,6), signal_out(1,7) signal_out(1,8), signal_out(1,9) ];


u_k_q = u_k_q.';
 
PA_out = A'*u_k_q ;


     case 4
         
         % ideal power amplifier
  
      A = [ [1 0 0]  [0 0 0]  [0 0 0] ]; A= A.';
      

signal_out = zeros(1,9);



% signal_out =  [ a(n),  a(n)|a(n)|^2, a(n)|a(n)|^4, a(n-1),  a(n-1)|a(n-1)|^2,a(n-1)|a(n-1)|^4,  a(n-2),  a(n-2)|a(n-2)|^2, a(n-2)|a(n-2)|^4 ]^T
       signal_out(1,1) = PA_in* abs((PA_in)).^(2*(0));
       signal_out(1,2) = PA_in* abs((PA_in)).^(2*(1));
       signal_out(1,3) = PA_in* abs((PA_in)).^(2*(2));
       
       signal_out(1,4) = PA_in2* abs((PA_in2)).^(2*(0));
       signal_out(1,5) = PA_in2* abs((PA_in2)).^(2*(1));
       signal_out(1,6) = PA_in2* abs((PA_in2)).^(2*(2));
       
       signal_out(1,7) = PA_in3* abs((PA_in3)).^(2*(0));
       signal_out(1,8) = PA_in3* abs((PA_in3)).^(2*(1));
       signal_out(1,9) = PA_in3* abs((PA_in3)).^(2*(2));
       
       
       
        

u_k_q = [signal_out(1,1),  signal_out(1,2), signal_out(1,3), signal_out(1,4), signal_out(1,5),signal_out(1,6), signal_out(1,7) signal_out(1,8), signal_out(1,9) ];


u_k_q = u_k_q.';
 
PA_out = A'*u_k_q; 



 end
    
           
 
 

