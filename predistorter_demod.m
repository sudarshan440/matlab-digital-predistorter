function [PD_out,u_k_q_PD] = predistorter_demod(predistorter_in,predistorter_in_2,predistorter_in_3,b_q_m)


 

signal_out = zeros(9,1);



% signal_out =  [ a(n),  a(n)|a(n)|^2, a(n)|a(n)|^4, a(n-1),  a(n-1)|a(n-1)|^2,a(n-1)|a(n-1)|^4,  a(n-2),  a(n-2)|a(n-2)|^2, a(n-2)|a(n-2)|^4 ]^T
       signal_out(1,1) = predistorter_in* (abs((predistorter_in)).^(2*(0)));
       signal_out(2,1) = predistorter_in* (abs((predistorter_in)).^(2*(1)));
       signal_out(3,1) = predistorter_in* (abs((predistorter_in)).^(2*(2)));
      
       signal_out(4,1) = predistorter_in_2* (abs((predistorter_in_2)).^(2*(0)));
       signal_out(5,1) = predistorter_in_2* (abs((predistorter_in_2)).^(2*(1)));
       signal_out(6,1) = predistorter_in_2* (abs((predistorter_in_3)).^(2*(2)));
       
       signal_out(7,1) = predistorter_in_3* (abs((predistorter_in_3)).^(2*(0)));
       signal_out(8,1) = predistorter_in_3* (abs((predistorter_in_3)).^(2*(1)));
       signal_out(9,1) = predistorter_in_3* (abs((predistorter_in_3)).^(2*(2)));
       
       
       
       
% taking only the non zero terms       
u_k_q_PD = signal_out;

PD_out= b_q_m'*u_k_q_PD; % from equation (8) in paper "A memory polynomial predistorter for compensation of nonlinearity with...
%memory effects in WCDMA transmitters, 2009 IEEE" 